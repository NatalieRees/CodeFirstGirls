#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 15 10:44:03 2023

@author: natalierees

Python script to analyse sales data from sales_dataset.csv

1. Use Pandas to read in the sales data from the CSV file sales_dataset.csv
2. Calculate the total sales for each product 
3. Determine the average sale price for each product category 
4. Identify the month with the highest sales and the month with the lowest sales
5. Determine which customers made the most purchases and how much they spent in total
6. Write the results of your analysis as a CSV file

"""

import calendar
import statistics
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

# matplotlib settings
mpl.rcParams["font.size"]=12
patterns = ("\\", "", "/",'',"\\", "", "/",'',"\\", "", "/",'' )

def read_csv_file(filename):
    return pd.read_csv(filename)

def calc_total_sales(df):
    '''Calculate total sales from dataframe and add as column to dataframe'''
    
    # total sales per product = sale price * quantity sold
    total_sales = df['Sale Price']*df['Quantity Sold']
    
    # add column to  dataframe
    df['Total Sales'] = round(total_sales,2)
    
    return df

def calc_average_sale_price_per_category(df):
    '''  Caculate the average sales price for each product category '''
    
    # make list of dictionaries for storing the average sale prices
    average_sale_price_list = []
    
    # loop through all categories and add results to dictionary
    for category in np.unique(df['Category']):
        # extract data for category
        cat_df = df.loc[df['Category']==category]
        # average sale price
        cat_average_sale_price = round(statistics.mean(cat_df['Sale Price']),2)
        # median sale price
        cat_median_sale_price = round(statistics.median(cat_df['Sale Price']),2)
        # sale price standard deviation, if only one point set equal to 0
        if len(cat_df) > 1:
            cat_stdev_sale_price = round(statistics.stdev(cat_df['Sale Price']),2)
        else:
            cat_stdev_sale_price = 0
        # add results to list
        average_sale_price_list += [{
            'Category':category,
            'Average Sale Price': cat_average_sale_price,
            'Median Sale Price': cat_median_sale_price,
            'Standard Deviation': cat_stdev_sale_price
            }]
            
    # convert list of dictionaries into dataframe
    categories_df = pd.DataFrame(average_sale_price_list)
    
    # sort by number of purchases from highest to lowest
    categories_df = categories_df.sort_values('Average Sale Price',ascending=False)
    
    # plot sale price metrics
    fig,ax=plt.subplots(figsize = (16,8))
    ax.scatter(categories_df['Category'],categories_df['Average Sale Price'],
               label='Mean Sale Price', marker = 'o',s=100)
    ax.scatter(categories_df['Category'],categories_df['Median Sale Price'],
               label ='Median Sale Price',marker = 'x',s=100)
    ax.scatter(categories_df['Category'],categories_df['Standard Deviation'],
               label ='Standard Deviation',marker = '+',s=100)
    ax.set(xlabel='Category Name',ylabel='Sale Price (£)')
    ax.legend()
    plt.title('Category Sale Price Metrics')
    plt.show()
    plt.savefig('category_sales.pdf',dpi=200)

    return categories_df

def calc_sales_per_month(df):
    ''' Calculate monthly sales '''
        
    # make list of dictionaries for storing the monthly sales
    monthly_sales_list = []
    
    # loop through all months and add results to dictionary
    for month in calendar.month_name[1:]:
        # extract data for month
        month_df = df.loc[df['Month'].str.strip()==month]
        # add up all sales
        monthly_total_sales = np.sum(month_df['Total Sales'])
        # add results to list
        monthly_sales_list += [{
            'Month':month,
            'Total Sales':round(monthly_total_sales,2)
            }]
        
    # convert list of dictionaries into dataframe
    monthly_sales_df = pd.DataFrame(monthly_sales_list)
    
    # sort by number of purchases from highest to lowest
    ordered_monthly_sales_df = monthly_sales_df.sort_values('Total Sales',ascending=False)

    # find months with highest and lowest sales
    highest_sales_month = ordered_monthly_sales_df.iloc[0]['Month']
    lowest_sales_month = ordered_monthly_sales_df.iloc[-1]['Month']
    
    return monthly_sales_df, highest_sales_month, lowest_sales_month

def calc_customers_purchases(df):
    ''' Calculate total customers purchases '''
    
    # make list of dictionaries for storing customers purchases
    customers_list = []
    
    # loop through all customers and save number of purchases and total money spent
    for customer in np.unique(df['Customer Name']):
        # extract data for customer
        cust_df = df.loc[df['Customer Name']==customer]
        # add up number of purchases and total money spent
        num_purchases = len(cust_df)
        total_spent = np.sum(cust_df['Total Sales'])
        # add results to list
        customers_list += [{
            'Customer Name':customer,
            'Number Purchases':num_purchases,
            'Total Money Spent':round(total_spent,2)
            }]
    
    # convert list of dictionaries into dataframe
    customers_df = pd.DataFrame(customers_list)
    
    # sort by total money spent
    customers_df = customers_df.sort_values('Total Money Spent',ascending=False)
    
    # find customers who made the most purchases
    max_num_purchases = max(customers_df['Number Purchases'])
    customers_most_purchases_df = customers_df.loc[customers_df['Number Purchases']==max_num_purchases]
        
    return customers_df, customers_most_purchases_df

def plot_category_sales_each_month(df):
    # plot total monthly sales as a bar chart seperated into product categories
    width = 0.9
    fig, ax = plt.subplots(figsize = (12,8))
    bottom = np.zeros(12)
    i=0
    # loop through categories and calculate total sales each month
    for cat in np.unique(df['Category']):
        cat_df = df.loc[df['Category']==cat]
        sales_list = np.array([
            np.sum(cat_df.loc[(df['Month'].str.strip()==month)]['Total Sales']) 
            for month in calendar.month_name[1:]
            ])
        # plot monthly sales for given category on top of previous categories
        ax.bar(calendar.month_abbr[1:], sales_list, width, label=cat, bottom=bottom,hatch=patterns[i])
        bottom += sales_list
        i+=1
    ax.legend(loc=(0.3,0.49))
    ax.set(xlabel='Month',ylabel='Total Sales (£)')
    ax.set_ylim(0,7000)
    plt.title('Total Monthly Sales')
    plt.show()
    plt.savefig('monthly_sales.pdf',dpi=200)
    
    return

if __name__ == '__main__':

    #################################################################
    # 1. Read in sales data to a pandas dataframe
    
    sales_df = read_csv_file('sales_dataset.csv')
    
    #################################################################
    # 2. Calculate total sales
    
    sales_df = calc_total_sales(df=sales_df)
    
    # print results
    print('\nTotal sales for each item:')
    print(sales_df)
        
    #################################################################
    # 3. Calculate the average sales price for each product category
    
    categories_df = calc_average_sale_price_per_category(df=sales_df)
    
    # print results
    print('\nAverage sale price per category:')
    print(categories_df)

    #################################################################
    # 4. Identify the month with the highest sales and the month with the lowest sales
    
    monthly_sales_df, highest_month, lowest_month = calc_sales_per_month(df=sales_df)
    
    # print results
    print('\nMonthly total sales:') 
    print(monthly_sales_df)
    
    #################################################################
    # 5. Determine which customers made the most purchases and how much they spent in total

    customers_df, customers_most_purchases_df = calc_customers_purchases(df=sales_df)
    
    # print results
    print('\nCustomers spending:') 
    print(customers_df)
    
    #################################################################
    # 6. Write results to csv file
    
    analysis_filename = 'analysis.csv'
    
    sales_df[['Product Name','Total Sales']].to_csv(analysis_filename)
    # add an empty line for separation
    pd.DataFrame({}).to_csv(analysis_filename,mode='a')

    categories_df.to_csv(analysis_filename,mode='a')
    # add an empty line for separation
    pd.DataFrame({}).to_csv(analysis_filename,mode='a')
    
    with open(analysis_filename,'a') as f:
        f.write(f',Month with highest sales:, {highest_month}\n')
        f.write(f',Month with lowest sales:, {lowest_month}\n')
    # add an empty line for separation
    pd.DataFrame({}).to_csv(analysis_filename,mode='a')
    
    customers_most_purchases_df.to_csv(analysis_filename,mode='a')

    #################################################################
    # 7. Extra: plot total monthly sales as a bar chart seperated into product categories
    
    plot_category_sales_each_month(df = sales_df)






