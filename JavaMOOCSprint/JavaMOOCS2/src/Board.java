import java.util.Scanner;

public class Board {
    private char[][] board;
    private char defaultCellValue = '*';
    public Board(){
        System.out.println("Creating board");
        createBoard();
    }

    public void displayBoard(){
        for (int row=0; row<3; row++){
            for (int col = 0; col<3; col++){
                System.out.println("row:"+row+", col:"+col+ " | "+board[row][col]);
            }
        }
    }
    private void createBoard(){
        board = new char[3][3];
        for (int row=0; row<3; row++){
            for (int col = 0; col<3; col++){
                board[row][col] = defaultCellValue;
            }
        }
    }


    public static void main(String[] args) {

        Board testBoard = new Board();


    }
}


