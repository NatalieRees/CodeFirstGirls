import java.util.Scanner;

public class demo_functions {

    // write a function that asks the user for name
    // get user input
    // display
    public static String GetName(int UserNumber){
        Scanner inputGetter = new Scanner(System.in);
        String AskForName = "What is your name (user "+UserNumber+")?";
        String UserName;

        System.out.println(AskForName);
        UserName = inputGetter.nextLine();
        return UserName;
    }

    public static char[][] createBoard(){
        char[][] board = new char[3][3];
        for (int row=0; row<3; row++){
            for (int col = 0; col<3; col++){
                board[row][col] = (char)('a'+col+row);
            }
        }
        return board;

    }

    public static void displayBoard(char[][] board){
        for (int row=0; row<3; row++){
            for (int col = 0; col<3; col++){
                System.out.println("row:"+row+", col:"+col+ " | "+board[row][col]);
            }
        }
    }

    public static void gameLoop(){

        char[][] board = createBoard();

        Scanner input = new Scanner(System.in);
        System.out.println("Do you want to display the board?");
        String userAnswer = input.nextLine();

        while (userAnswer.equals("yes")){
            displayBoard(board);
            System.out.println("Do you want to display the board?");
            userAnswer = input.nextLine();
        }
        System.out.println("Bye");
    }
    public static void main(String[] args) {
        int number = 2;
//        String username = GetName(number);
//        System.out.println("Hello "+username);

//        int[] array1 = createArray();
//
//        displayArray(array1);
//
//        char[][] board = createBoard();
//        displayBoard(board);

        gameLoop();
    }
}
