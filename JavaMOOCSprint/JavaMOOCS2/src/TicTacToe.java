import java.util.Scanner;

public class TicTacToe {
    Board tBoard;
    public TicTacToe(){
        System.out.println("Welcome to TicTacToe");
        tBoard = new Board();
    }
    public void gameLoop(){

        Scanner input = new Scanner(System.in);
        System.out.println("Do you want to display the board?");
        String userAnswer = input.nextLine();

        while (userAnswer.equals("yes")){
            tBoard.displayBoard();
            System.out.println("Do you want to display the board?");
            userAnswer = input.nextLine();
        }
        System.out.println("Bye");
    }

    public static void main(String[] args) {
//        TicTacToe tt = new TicTacToe();
//        tt.gameLoop();

        User auser = new User();
        auser.setUsername("CFG");
        auser.setMarker('x');
        System.out.println(auser.getUsername());
        System.out.println(auser.getMarker());
    }
}
